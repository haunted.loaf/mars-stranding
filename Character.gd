extends KinematicBody

var speed = 10
var fastSpeed = 50
var acceleration = 5
var velocity = Vector3()
var direction = Vector3()
var look_speed = 0.1

func _ready():
  Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
  pass

func _input(event):
  if event is InputEventMouseMotion:
    rotate_y(deg2rad(-event.relative.x * look_speed))
    $Camera.rotate_x(deg2rad(-event.relative.y * look_speed))

func _process(delta):
  direction = Vector3()
  if Input.is_action_pressed("move_forward"):
    direction -= transform.basis.z
  if Input.is_action_pressed("move_back"):
    direction += transform.basis.z
  if Input.is_action_pressed("move_left"):
    direction -= transform.basis.x
  if Input.is_action_pressed("move_right"):
    direction += transform.basis.x
  if Input.is_action_pressed("move_up"):
    direction += transform.basis.y
  if Input.is_action_pressed("move_down"):
    direction -= transform.basis.y
  if Input.is_action_pressed("ui_cancel"):
    get_tree().quit()
  direction = direction.normalized()
  velocity = direction * speed
  if Input.is_key_pressed(KEY_SHIFT):
    velocity = direction * fastSpeed
  velocity.linear_interpolate(velocity, acceleration * delta)
  move_and_slide(velocity, Vector3.UP)
