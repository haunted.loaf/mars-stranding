﻿using System;
using System.Collections.Generic;
using Godot;

namespace QuadTerrain
{

  public class NodeMesh : MeshInstance
  {

    [Export]
    public TreeNode node;

    [Export]
    public bool dirty = true;

    public static NodeMesh New(TreeNode node)
    {
      var mesh = New(node.terrain.nodeMeshPrefab);
      node.AddChild(mesh);
      return mesh;
    }

    public static NodeMesh New(PackedScene prefab) => prefab.Instance() as NodeMesh;

    // public MeshFilter filter;
    // new public MeshRenderer renderer;
    // new public MeshCollider collider;

    // public void Destroy () {
    //   DestroyImmediate(gameObject);
    // }

    public ArrayMesh CreateMesh(VertexData data, List<int> indices)
    {
      var mesh = new ArrayMesh();
      // var st = new SurfaceTool();
      // st.Begin(Mesh.PrimitiveType.Triangles);
      // foreach (var i in indices) {
      // //   st.AddUv(data.UV0[i]);
      // //   st.AddUv2(data.UV1[i]);
      //   st.AddVertex(data.Position[i]);
      // }
      // st.AddVertex(new Vector3(0, 1, 0));
      // st.AddVertex(new Vector3(1, 0, 0));
      // st.AddVertex(new Vector3(0, 0, 1));
      // st.AddVertex();
      // st.Commit(mesh);
      var arrays = new Godot.Collections.Array();
      arrays.Resize((int)ArrayMesh.ArrayType.Max);
      arrays[(int)ArrayMesh.ArrayType.Vertex] = data.Position;
      arrays[(int)ArrayMesh.ArrayType.Normal] = data.Position.ToArray();
      arrays[(int)ArrayMesh.ArrayType.TexUv] = data.UV0;
      arrays[(int)ArrayMesh.ArrayType.TexUv2] = data.UV1;
      arrays[(int)ArrayMesh.ArrayType.Index] = indices;
      mesh.AddSurfaceFromArrays(Mesh.PrimitiveType.Triangles, arrays);
      mesh.RegenNormalmaps();
      return mesh;
    }

    public void Setup(TreeNode node, HeightProvider heightProvider)
    {
      dirty = false;
      this.node = node;
      var data = ComputeVertexData(heightProvider);
      var mesh = CreateMesh(data, node.terrain.indexBuffers[node.activeIndexBuffer]);
      mesh.SurfaceSetMaterial(0, node.terrain.material);
      mesh.SurfaceSetName(0, "Terrain");
      // mesh.GenerateTriangleMesh();
      this.Mesh = mesh;
      this.MaterialOverride = node.terrain.material;
    }

    public VertexData ComputeVertexData(HeightProvider heightProvider)
    {
      var data = new VertexData();
      var offsetNorth = node.centrePoint.z + node.halfEdgeLength;
      var offsetWest = node.centrePoint.x - node.halfEdgeLength;
      var vertexSpacing = node.edgeLength / (node.terrain.verticesPerEdge - 1);
      for (var y = 0; y < node.terrain.verticesPerEdge; y++)
      {
        for (var x = 0; x < node.terrain.verticesPerEdge; x++)
        {
          var positionX = offsetWest + (x * vertexSpacing);
          var positionZ = offsetNorth - (y * vertexSpacing);
          data.Position.Add(new Vector3(
              positionX,
              heightProvider.GetHeight(positionX / node.terrain.size, positionZ / node.terrain.size, node.terrain.size),
              positionZ
            ) - node.centrePoint);
          data.UV0.Add(new Vector2(
              (positionX / node.terrain.size) + 0.5f,
              (positionZ / node.terrain.size) + 0.5f
            ));
          data.UV1.Add(new Vector2(
              (node.terrain.textureScale * (positionX / node.terrain.size)) + 0.5f,
              (node.terrain.textureScale * (positionZ / node.terrain.size)) + 0.5f
            ));
        }
      }
      return data;
    }

    public static Dictionary<IndexBufferSelection, List<int>> CreateIndexBuffers(int verticesPerEdge)
    {
      return new Dictionary<IndexBufferSelection, List<int>> {
        { IndexBufferSelection.Base,       CreateIndexBuffer(verticesPerEdge, false, false, false, false) },
        { IndexBufferSelection.NCrackFix,  CreateIndexBuffer(verticesPerEdge, true, false, false, false) },
        { IndexBufferSelection.ECrackFix,  CreateIndexBuffer(verticesPerEdge, false, true, false, false) },
        { IndexBufferSelection.SCrackFix,  CreateIndexBuffer(verticesPerEdge, false, false, true, false) },
        { IndexBufferSelection.WCrackFix,  CreateIndexBuffer(verticesPerEdge, false, false, false, true) },
        { IndexBufferSelection.NwCrackFix, CreateIndexBuffer(verticesPerEdge, true, false, false, true) },
        { IndexBufferSelection.NeCrackFix, CreateIndexBuffer(verticesPerEdge, true, true, false, false) },
        { IndexBufferSelection.SeCrackFix, CreateIndexBuffer(verticesPerEdge, false, true, true, false) },
        { IndexBufferSelection.SwCrackFix, CreateIndexBuffer(verticesPerEdge, false, false, true, true) }
      };
    }

    public static List<int> CreateIndexBuffer(int verticesPerEdge, bool northCrackFix, bool eastCrackFix, bool southCrackFix, bool westCrackFix)
    {
      // Resulting triangles are wound CCW
      var indices = new List<int>();
      for (var y = 0; y < verticesPerEdge - 1; y++)
      {
        var slantLeft = y % 2 == 0;
        for (var x = 0; x < verticesPerEdge - 1; x++)
        {
          var nwIndex = (int)(x + (y * verticesPerEdge));
          var neIndex = (int)(x + 1 + (y * verticesPerEdge));
          var seIndex = (int)(x + 1 + ((y + 1) * verticesPerEdge));
          var swIndex = (int)(x + ((y + 1) * verticesPerEdge));
          var triangle1 = slantLeft ? new int[3] { nwIndex, swIndex, seIndex } : new int[3] { nwIndex, swIndex, neIndex };
          var triangle2 = slantLeft ? new int[3] { nwIndex, seIndex, neIndex } : new int[3] { swIndex, seIndex, neIndex };

          // Perform requested crack fixing
          if (northCrackFix && y == 0)
          {
            if (x % 2 == 0)
              triangle2 = new int[3] { nwIndex, seIndex, neIndex + 1 };
            else
              triangle1 = null;
          }
          if (eastCrackFix && x == verticesPerEdge - 2)
          {
            if (y % 2 == 0)
              triangle2 = new int[3] { neIndex, swIndex, seIndex + verticesPerEdge };
            else
              triangle2 = null;
          }
          if (southCrackFix && y == verticesPerEdge - 2)
          {
            if (x % 2 == 0)
              triangle2 = new int[3] { swIndex, seIndex + 1, neIndex };
            else
              triangle1 = null;
          }
          if (westCrackFix && x == 0)
          {
            if (y % 2 == 0)
              triangle1 = new int[3] { nwIndex, swIndex + verticesPerEdge, seIndex };
            else
              triangle1 = null;
          }

          if (triangle1 != null)
            indices.AddRange(triangle1);
          if (triangle2 != null)
            indices.AddRange(triangle2);

          slantLeft = !slantLeft;
        }
      }

      return indices;
    }

  }

}
