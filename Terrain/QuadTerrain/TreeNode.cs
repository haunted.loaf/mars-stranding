﻿using System;
using System.Linq;
using Godot;

namespace QuadTerrain {

  [Serializable]
  public class TreeNode : Spatial {

    public Terrain terrain;

    public NodeType nodeType;
    public IndexBufferSelection activeIndexBuffer;

    public TreeNode parentNode;

    public float edgeLength;
    public float halfEdgeLength;
    public float quarterEdgeLength;

    public Vector3 centrePoint;

    public bool hasChildren;

    public TreeNode childNorthEast;
    public TreeNode childSouthEast;
    public TreeNode childSouthWest;
    public TreeNode childNorthWest;

    public TreeNode neighbourNorth;
    public TreeNode neighbourEast;
    public TreeNode neighbourSouth;
    public TreeNode neighbourWest;

    public NodeMesh mesh;

    public TreeNode Setup (Terrain terrain, NodeType nodeType, TreeNode parentNode, float centreX, float centreZ, float edgeLength) {

      terrain.AddChild(this);

      // if (!terrain)
      //   throw new ArgumentException("A node must belong to a parent");
      // if (parentNode == null && nodeType != NodeType.Root)
      //   throw new ArgumentException("A non-root node must have a parent");

      var name = "R";

      if (parentNode != null)
        name = parentNode.Name;

      switch (nodeType) {
      case NodeType.NorthEast:
        name += "0";
        break;
      case NodeType.SouthEast:
        name += "1";
        break;
      case NodeType.SouthWest:
        name += "2";
        break;
      case NodeType.NorthWest:
        name += "3";
        break;
      }

      this.Name = name;

      this.terrain = terrain;

      this.parentNode = parentNode;

      this.edgeLength = edgeLength;
      halfEdgeLength = this.edgeLength / 2;

      this.nodeType = nodeType;

      centrePoint = new Vector3(centreX, 0, centreZ);

      quarterEdgeLength = this.edgeLength / 4;

      activeIndexBuffer = IndexBufferSelection.Base;

      TranslateObjectLocal(centrePoint);

      return this;

    }

    public static TreeNode New (Terrain terrain) => New(terrain.treeNodePrefab);

    public static TreeNode New (PackedScene prefab) => prefab.Instance() as TreeNode;

    public void UpdateChildrenRecursively (Vector3 observer) {
      var position = new Vector3(centrePoint.x, 0, centrePoint.z);
      var minDistance = (position - observer).Length();
      if (halfEdgeLength > Mathf.Max(terrain.edgeLengthLimit, 0.01f)
          && minDistance < terrain.splitDistanceMultiplier * edgeLength) {
        if (!hasChildren) {
          childNorthEast = TreeNode.New(terrain).Setup(terrain, NodeType.NorthEast, this, centrePoint.x + quarterEdgeLength, centrePoint.z + quarterEdgeLength, halfEdgeLength);
          childSouthEast = TreeNode.New(terrain).Setup(terrain, NodeType.SouthEast, this, centrePoint.x + quarterEdgeLength, centrePoint.z - quarterEdgeLength, halfEdgeLength);
          childSouthWest = TreeNode.New(terrain).Setup(terrain, NodeType.SouthWest, this, centrePoint.x - quarterEdgeLength, centrePoint.z - quarterEdgeLength, halfEdgeLength);
          childNorthWest = TreeNode.New(terrain).Setup(terrain, NodeType.NorthWest, this, centrePoint.x - quarterEdgeLength, centrePoint.z + quarterEdgeLength, halfEdgeLength);
          hasChildren = true;
        }
        childNorthWest.UpdateChildrenRecursively(observer);
        childNorthEast.UpdateChildrenRecursively(observer);
        childSouthEast.UpdateChildrenRecursively(observer);
        childSouthWest.UpdateChildrenRecursively(observer);
        UpdateVisibility();
      } else if (hasChildren) {
        DestroyChildren();
        UpdateVisibility();
      }
    }

    public void DestroyChildren () {
      childNorthEast?.Destroy();
      childSouthEast?.Destroy();
      childSouthWest?.Destroy();
      childNorthWest?.Destroy();
      hasChildren = false;
    }

    public void UpdateVisibility () {
      if (mesh != null) {
        mesh.Visible = !hasChildren;
        mesh.SetProcess(!hasChildren);
      }
    }

    public void UpdateNeighboursRecursively () {
      switch (nodeType) {
      case NodeType.NorthWest:
        if (parentNode.neighbourNorth != null)
          neighbourNorth = parentNode.neighbourNorth.childSouthWest;
        neighbourEast = parentNode.childNorthEast;
        neighbourSouth = parentNode.childSouthWest;
        if (parentNode.neighbourWest != null)
          neighbourWest = parentNode.neighbourWest.childNorthEast;
        break;

      case NodeType.NorthEast:
        if (parentNode.neighbourNorth != null)
          neighbourNorth = parentNode.neighbourNorth.childSouthEast;
        if (parentNode.neighbourEast != null)
          neighbourEast = parentNode.neighbourEast.childNorthWest;
        neighbourSouth = parentNode.childSouthEast;
        neighbourWest = parentNode.childNorthWest;
        break;

      case NodeType.SouthEast:
        neighbourNorth = parentNode.childNorthEast;
        if (parentNode.neighbourEast != null)
          neighbourEast = parentNode.neighbourEast.childSouthWest;
        if (parentNode.neighbourSouth != null)
          neighbourSouth = parentNode.neighbourSouth.childNorthEast;
        neighbourWest = parentNode.childSouthWest;
        break;

      case NodeType.SouthWest:
        neighbourNorth = parentNode.childNorthWest;
        neighbourEast = parentNode.childSouthEast;
        if (parentNode.neighbourSouth != null)
          neighbourSouth = parentNode.neighbourSouth.childNorthWest;
        if (parentNode.neighbourWest != null)
          neighbourWest = parentNode.neighbourWest.childSouthEast;
        break;
      }

      if (hasChildren) {
        childNorthWest.UpdateNeighboursRecursively();
        childNorthEast.UpdateNeighboursRecursively();
        childSouthWest.UpdateNeighboursRecursively();
        childSouthEast.UpdateNeighboursRecursively();
      }
    }

    // public void DestroyMesh () {
    //   if (mesh)
    //     GameObject.DestroyImmediate(mesh.gameObject);
    //   mesh = null;
    // }

    public void Destroy () {
    //   DestroyChildren();
    //   DestroyMesh();
    //   GameObject.DestroyImmediate(gameObject);
    }

    public void ForEach (Action<TreeNode> f) {
      f(this);
      childNorthEast?.ForEach(f);
      childSouthEast?.ForEach(f);
      childSouthWest?.ForEach(f);
      childNorthWest?.ForEach(f);
    }

    internal void CreateMesh (HeightProvider heightProvider) {
      if (mesh == null)
        mesh = NodeMesh.New(this);
      if (mesh.dirty)
        mesh.Setup(this, heightProvider);
    }

    public void FixCracks () {
      var newIndexBuffer = IndexBufferSelection.Base;

      switch (nodeType) {
      case NodeType.NorthWest:
        if (neighbourNorth == null && neighbourWest == null) {
          newIndexBuffer = IndexBufferSelection.NwCrackFix;
        } else if (neighbourNorth == null) {
          newIndexBuffer = IndexBufferSelection.NCrackFix;
        } else if (neighbourWest == null) {
          newIndexBuffer = IndexBufferSelection.WCrackFix;
        }
        break;

      case NodeType.NorthEast:
        if (neighbourNorth == null && neighbourEast == null) {
          newIndexBuffer = IndexBufferSelection.NeCrackFix;
        } else if (neighbourNorth == null) {
          newIndexBuffer = IndexBufferSelection.NCrackFix;
        } else if (neighbourEast == null) {
          newIndexBuffer = IndexBufferSelection.ECrackFix;
        }
        break;

      case NodeType.SouthEast:
        if (neighbourSouth == null && neighbourEast == null) {
          newIndexBuffer = IndexBufferSelection.SeCrackFix;
        } else if (neighbourSouth == null) {
          newIndexBuffer = IndexBufferSelection.SCrackFix;
        } else if (neighbourEast == null) {
          newIndexBuffer = IndexBufferSelection.ECrackFix;
        }
        break;

      case NodeType.SouthWest:
        if (neighbourSouth == null && neighbourWest == null) {
          newIndexBuffer = IndexBufferSelection.SwCrackFix;
        } else if (neighbourSouth == null) {
          newIndexBuffer = IndexBufferSelection.SCrackFix;
        } else if (neighbourWest == null) {
          newIndexBuffer = IndexBufferSelection.WCrackFix;
        }
        break;
      }

      if (mesh != null && activeIndexBuffer != newIndexBuffer)
        mesh.dirty = true;

      activeIndexBuffer = newIndexBuffer;

    }

  }

}
