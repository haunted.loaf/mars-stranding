﻿using Godot;
using System;
using System.Collections.Generic;

namespace QuadTerrain
{

  public class VertexData
  {
    public List<Vector3> Position = new List<Vector3>();
    public List<Vector2> UV0 = new List<Vector2>();
    public List<Vector2> UV1 = new List<Vector2>();
  }

}
