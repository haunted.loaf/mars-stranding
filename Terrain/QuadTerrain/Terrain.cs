﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using Godot;

namespace QuadTerrain
{

  public class Terrain : Spatial
  {

    [Export]
    public int verticesPerEdge = 5;

    [Export]
    public float edgeLengthLimit = 10;

    [Export]
    public float splitDistanceMultiplier = 4;

    [Export]
    public float normalSampleOffset = 1;

    [Export]
    public float normalPower;

    [Export]
    public NodePath heightProvider;

    [Export]
    public float size = 10;

    [Export]
    public Vector3 terrainCentrePosition;

    [Export]
    public TreeNode root;

    [Export]
    public PackedScene treeNodePrefab;

    [Export]
    public PackedScene nodeMeshPrefab;

    [Export]
    public bool recalculateTangents;

    [Export]
    public bool recalculateNormals;

    [Export]
    public Dictionary<IndexBufferSelection, List<int>> indexBuffers;

    [Export]
    public float textureScale = 1000;

    [Export]
    public Material material;

    public override void _Ready()
    {
      indexBuffers = NodeMesh.CreateIndexBuffers(verticesPerEdge);
      BuildTree();
    }

    public override void _Process(float delta)
    {
      var hp = GetHeightProvider();
      UpdateChildrenRecursively();
      UpdateNeighboursRecursively();
      root.ForEach(node => {
        node.FixCracks();
        node.CreateMesh(hp);
        node.UpdateVisibility();
      });
    }

    public void BuildTree () {
      root = TreeNode.New(treeNodePrefab).Setup(this, NodeType.Root, null, 0, 0, size);
    }

    public void UpdateNeighboursRecursively () => root.UpdateNeighboursRecursively();

    public void UpdateChildrenRecursively () {
      var observer = GetViewport().GetCamera().GlobalTransform.origin;
      root.UpdateChildrenRecursively(observer);
    }

    public HeightProvider GetHeightProvider () {
      return GetNode(heightProvider) as HeightProvider;
    }

    //     public void Start () => dirty = true;
    //     public void OnValidate () => dirty = true;
    //     public void OnHeightProviderChanged () => dirty = true;

    //     public void OnEnable () {
    //       if (heightProvider == null)
    //         heightProvider.OnChanged.AddListener(OnHeightProviderChanged);
    //     }

    //     public void OnDisable () {
    //       if (heightProvider != null)
    //         heightProvider.OnChanged.RemoveListener(OnHeightProviderChanged);
    //     }

    //     public void Clear () {
    //     }

  }

}
