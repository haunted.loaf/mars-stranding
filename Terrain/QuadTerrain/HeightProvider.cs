﻿using System;
using Godot;

namespace QuadTerrain {

  public class HeightProvider : Node {

    [Export]
    public float scale;

    [Export]
    public Texture heights;

    private Image image;

    // public UnityEvent OnChanged;

    // public void OnValidate () {
    //   OnChanged.Invoke();
    // }

    public float GetHeight (float x, float z, float scale) {
      if (image == null) {
        image = heights.GetData();
        image.Lock();
      }
      var y = image.GetPixel((int) ((heights.GetWidth() - 1) * (x + 0.5f)), (int) ((heights.GetHeight() - 1) * (z + 0.5f))).r * scale * this.scale;
      return y;
    }

    // public Vector3 GetNormal (float x, float z, float scale, float offset) {
    //   var uvOffset = heights.texelSize * offset;
    //   var l = GetHeight(x - uvOffset.x, z, 1);
    //   var r = GetHeight(x + uvOffset.x, z, 1);
    //   var b = GetHeight(x, z - uvOffset.y, 1);
    //   var t = GetHeight(x, z + uvOffset.y, 1);
    //   return new Vector3(
    //     scale * (r - l),
    //     1,
    //     scale * (t - b)
    //   ).normalized;
    // }

    // public float3 GetNormal(float2 uv, float2 texelSize, float scale) {
    //   CalculateUVsSmooth(
    //     uv, texelSize,
    //     out var uv0, out var uv1, out var uv2, out var uv3, out var uv5, out var uv6, out var uv7, out var uv8
    //   );
    //   return CombineSamplesSmooth(
    //     scale,
    //     GetHeight(uv0.x, uv0.y, 1),
    //     GetHeight(uv1.x, uv1.y, 1),
    //     GetHeight(uv2.x, uv2.y, 1),
    //     GetHeight(uv3.x, uv3.y, 1),
    //     GetHeight(uv5.x, uv5.y, 1),
    //     GetHeight(uv6.x, uv6.y, 1),
    //     GetHeight(uv7.x, uv7.y, 1),
    //     GetHeight(uv8.x, uv8.y, 1)
    //   );
    // }

    // void CalculateUVsSmooth (float2 uv, float2 texelSize, out float2 uv0, out float2 uv1, out float2 uv2, out float2 uv3, out float2 uv5, out float2 uv6, out float2 uv7, out float2 uv8) {
    //   float3 pos = float3(texelSize.xy, 0);
    //   float3 neg = float3(-pos.xy, 0);
    //   uv0 = uv + neg.xy;
    //   uv1 = uv + neg.zy;
    //   uv2 = uv + float2(pos.x, neg.y);
    //   uv3 = uv + neg.xz;
    //   uv5 = uv + pos.xz;
    //   uv6 = uv + float2(neg.x, pos.y);
    //   uv7 = uv + pos.zy;
    //   uv8 = uv + pos.xy;
    // }

    // float3 CombineSamplesSmooth (float strength, float s0, float s1, float s2, float s3, float s5, float s6, float s7, float s8) {
    //   float3 normal;
    //   normal.x = strength * (s0 - s2 + (2 * s3) - (2 * s5) + s6 - s8);
    //   normal.y = 1;
    //   normal.z = strength * (s0 + (2 * s1) + s2 - s6 - (2 * s7) - 8);
    //   return normalize(normal);
    // }

  }

}
